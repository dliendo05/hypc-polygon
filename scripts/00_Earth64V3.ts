import { HardhatRuntimeEnvironment } from 'hardhat/types';
import hre from "hardhat";
const { ethers, upgrades } = require("hardhat");

//For deploying in testnet:
// npx hardhat run ./scripts/00_Earth64V3.ts --network mumbai

async function main(hre: HardhatRuntimeEnvironment) {

  const {  network } = hre;

  //Oracle address depends on the network of deployment mumbai or polygon
  const oracleAddress = network.name == "mumbai" ? "0x0715A7794a1dc8e42615F059dD6e406A6594651A" : "0xF9680D99D6C9589e2a93a78A04A279e509205945"

  // Deploying upgradable contract
  const Earth64V3 = await ethers.getContractFactory("Earth64V3");
  const instance = await upgrades.deployProxy(Earth64V3, [oracleAddress]);
  await instance.deployed();

  console.log("Contract deployed to:", instance.address)
//   For Upgrade
//   const Earth64V4  = await ethers.getContractFactory("Earth64V4");
//   const upgraded = await upgrades.upgradeProxy(instance.address, Earth64V4 );
}

main(hre);