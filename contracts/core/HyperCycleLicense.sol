// SPDX-License-Identifier: MIT
/*
    Hypercycle License Contract
*/

pragma solidity ^0.8.2;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "../interfaces/AggregatorV3Interface.sol";

//Eth Price Oracle, Chainlink Service
interface EPO {
    function latestAnswer() external view returns (uint);
}

//Upgradeable ERC721 Contract, using Openzeppelin's UUPS upgradeable pattern
contract HyperCycleLicense is ERC721Upgradeable, UUPSUpgradeable, OwnableUpgradeable {
    /* V1 variables */
    uint64 public count;
    uint64 public startCount;

    //Careful with unset variables
    AggregatorV3Interface EPO; //Chainlink's free MATIC Price Oracle
    address withdrawAddress;
   
    uint public lastPrice;
    string public ipfsListData;
    uint public deployTime;
    mapping(uint256 => string) public burnData;
    uint32 public totalSupply;
    uint public postListStartAmount;

    //testnet not set
    bool private isTestnet;
    address mintTreasury; //address authorized to issue previous tokens.

    function initialize(AggregatorV3Interface _EPO) public initializer {
        __ERC721_init("HyperCycleLicense", "HC-L");
        __Ownable_init();
        EPO = _EPO;
        count = 8796629893120;
        startCount = 8796629893120;
    }

    function getNextPriceMATIC() public view virtual returns (uint256) {
        return getNFTPriceInWei(getNextPrice());
    }

    function getNextPrice() public view virtual returns (uint256) {
        return getPrice(count-startCount);
    }

    function purchase() public payable virtual {
        uint256 PriceUSD = getNextPrice();
        uint256 Price = getNFTPriceInWei(PriceUSD);
  
        require(count < 8796629893120+2**30, "Maximum number of tokens minted");
        require(msg.value >= Price, "Not enough ETH was provided");

        lastPrice = PriceUSD;
        _safeMint(msg.sender, count, "");
        count++;
        payable(msg.sender).transfer(msg.value - Price);
    }

    function getNFTPriceInWei(
        uint256 price
    ) public view virtual returns (uint256) {
        return ((10 ** 18) * price) / getMATICPriceInUSD();
    }

    receive() external payable virtual {
        purchase();
    }

    function getMATICPriceInUSD()
        public
        view
        virtual
        returns (uint256 chainlinkAnswer)
    {
        if (isTestnet) {
            return 252952000000;
        } else {
            try EPO.latestRoundData() returns (
                uint80 /* roundId*/,
                int256 answer,
                uint256 /* startedAt */,
                uint256 /*timestamp */,
                uint80 /* answeredInRound */
            ) {
                chainlinkAnswer = uint256(answer);
            } catch {
                // If call to Chainlink aggregator reverts, return a zero response with success = false
                return chainlinkAnswer;
            }
        }
    }

    function withdraw() public virtual {
        require(
            msg.sender == withdrawAddress,
            "Only withdrawAddress can withdraw"
        );
        payable(withdrawAddress).transfer(address(this).balance);
    }

    function burnTo(uint256 tokenId, string memory burnString) public virtual {
        address owner = ownerOf(tokenId);
        require(owner == msg.sender, "ERC721: must own token to burn it");
        burnData[tokenId] = burnString;
        _burn(tokenId);
    }

    function getBurnInfo(
        uint256 tokenId
    ) public view virtual returns (string memory) {
        return burnData[tokenId];
    }

    function _authorizeUpgrade(
        address newImplementation
    ) internal override onlyOwner {}


    //Splitting function:
    function splitToken(uint32 tokenId) public {
        address owner = ownerOf(tokenId);
        require(owner == msg.sender, "ERC721: must own token to split it");

        //burn this token id.
        _burn(tokenId);
        //create left child
        _safeMint(msg.sender, tokenId*2);
        //create right child
        _safeMint(msg.sender, tokenId*2+1);
    }

    // In wei
    function getPrice(uint256 best) public pure returns (uint256) {
        if (best < 13) {
            return 1 ether;
        } else if (best < 17) {
            return (1310720) * (10 ^ 16);
        } else {
            uint256 res = (1310720) * (10 ^ 16) * 2;
            uint256 discount = (495 - 5 * (best - 17)) * 1 ether / 1000;
            return (100 * (res * (1 - discount))) / 100;
        }
    }
}
