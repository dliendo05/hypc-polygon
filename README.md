# SingularityNet Smart Contracts

NFT with custom logic and methods for SingularityNet

Create .env file following the .env.example file.

For deploying Earth64V3 in mumbai testnet:

```shell
npx hardhat run ./scripts/00_Earth64V3.ts --network mumbai
```
