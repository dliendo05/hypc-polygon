import * as dotenv from "dotenv";
import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "@openzeppelin/hardhat-upgrades";
import "hardhat-contract-sizer";
dotenv.config();

const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  networks: {
    polygon: {
      url: process.env.POLYGON_RPC_PROVIDER,
      accounts: { mnemonic: process.env.MNEMONIC },
      timeout: 300000,
      saveDeployments: true,
      deploy: ["deploy/v1"],
    },
    mumbai: {
      url: process.env.MUMBAI_RPC_PROVIDER,
      accounts: { mnemonic: process.env.MNEMONIC },
      timeout: 300000,
      saveDeployments: true,
      deploy: ["deploy/v1"],
      gas: 2100000,
      gasPrice: 8000000000,
    },
  },
  etherscan: {
    apiKey: {
      polygon: `${process.env.POLYGON_SCAN_KEY}`,
      goerli: `${process.env.ETHERSCAN_KEY}`,
      polygonMumbai: `${process.env.POLYGON_SCAN_KEY}`,
    },
  },
  contractSizer: {
    alphaSort: false,
    disambiguatePaths: false,
    runOnCompile: true,
    strict: true,
    only: [],
  }
};

export default config;
